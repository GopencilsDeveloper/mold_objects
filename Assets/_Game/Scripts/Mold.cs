﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Mold : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS

    public List<GameObject> relaysList;
    public GameObject content;
    public GameObject core;
    public Animator animator;

    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void OnEnable()
    {
        relaysList.Clear();
        core.SetActive(false);
        relaysList.AddRange(GameObject.FindGameObjectsWithTag("Relay"));
        foreach (GameObject relay in relaysList)
        {
#if UNITY_EDITOR

            relay.GetComponent<MeshRenderer>().enabled = true;
#else
            relay.GetComponent<MeshRenderer>().enabled = false;
#endif
        }

    }

    public void OnWinGame()
    {
        core.SetActive(true);

        foreach (GameObject relay in relaysList)
        {
            relay.SetActive(false);
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
