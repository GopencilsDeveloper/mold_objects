﻿using System.Collections;
using System.Collections.Generic;
using Obi;
using UnityEngine;

public class MoldManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public List<Mold> moldsList;
    public Transform moldParent;

    #endregion

    #region PARAMS
    public static MoldManager Instance;
    public Mold currentMold;

    [HideInInspector] public Obi.ObiSolver solver;
    [HideInInspector] public Obi.ObiEmitter emitter;

    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
        solver = GameObject.FindObjectOfType<Obi.ObiSolver>();
        emitter = GameObject.FindObjectOfType<Obi.ObiEmitter>();
    }

    public void Refresh()
    {
        solver.gameObject.SetActive(true);
    }

    public void SpawnMold(int index = 0)
    {
        if (currentMold != null)
        {
            Destroy(currentMold.gameObject);
            currentMold = null;
        }

        if (index >= moldsList.Count)
        {
            index = (int)Random.Range(0f, moldsList.Count);
        }

        currentMold = Instantiate(moldsList[index], moldParent);
        currentMold.transform.position = Vector3.zero;
    }

    public void Win()
    {
        emitter.speed = 0f;
        currentMold.OnWinGame();
    }

    #endregion

    #region DEBUG
    #endregion

}
