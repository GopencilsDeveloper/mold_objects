﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConfiguration : MonoBehaviour
{
    public float fluidSpeed = 3f;

    [Tooltip("After fluidDelay seconds, fluid pours")]
    public float fluidDelay = 1f;

    [Tooltip("All relays is reached, then after relaysHoldingTime, game is Win")]
    public float relaysHoldingTime = 1f;

    public static GameConfiguration Instance;

    private void Awake()
    {
        Instance = this;
    }
}
