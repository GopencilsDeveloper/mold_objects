﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    #region CONST
    public const string LEVEL = "LEVEL";
    public const string SCORE = "SCORE";
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS
    private int currentLevel;
    #endregion

    #region PROPERTIES
    public static DataManager Instance { get; private set; }
    public int CurrentLevel
    {
        get
        {
            return PlayerPrefs.GetInt(LEVEL);
        }
        set
        {
            PlayerPrefs.SetInt(LEVEL, value);
        }
    }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS
    private void Awake()
    {
        Instance = this;
    }
    #endregion
}
