﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lid : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS

    private int collidedRelaysAmount;
    private int totalRelaysAmount;
    private float timer;

    private bool isReached;

    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void OnEnable()
    {
        isReached = false;
        collidedRelaysAmount = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Relay"))
        {
            collidedRelaysAmount++;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Relay"))
        {
            collidedRelaysAmount--;
        }
    }

    private void Update()
    {
        if (collidedRelaysAmount > 0 && collidedRelaysAmount == MoldManager.Instance.currentMold.relaysList.Count)
        {
            timer += Time.deltaTime;
            if (timer >= GameConfiguration.Instance.relaysHoldingTime && !isReached)
            {
                Debug.Log("Win!");
                isReached = true;
                timer = 0f;

                MoldManager.Instance.Win();
            }
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
