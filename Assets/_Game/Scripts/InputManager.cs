﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Obi;
using Lean.Touch;
using DG.Tweening;
public class InputManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public ObiEmitter emitter;
    public float emitterMinX;
    public float emitterMaxX;
    public Image timerFill;
    public bool canPour;
    public Transform mug;
    public Transform mugParent;
    public Transform pivot;

    #endregion

    #region PARAMS

    private float startTimer;
    private float deltaTimer;
    public static InputManager Instance;
    private Tween rotateMugTween;
    private int blueprintCapacity;

    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        canPour = true;
        emitter.gameObject.GetComponent<LeanSelectable>().IsSelected = true;
        blueprintCapacity = (int)emitter.emitterBlueprint.capacity;
    }

    private void Update()
    {
        if (!canPour)
        {
            return;
        }

        // if (emitter.activeParticleCount >= blueprintCapacity)
        // {
        //     Debug.Log("Out of Water!");
        // }

        // Mug LookAt Center
        pivot.position = new Vector3(0f, emitter.transform.position.y, emitter.transform.position.z + 1.5f);
        mugParent.LookAt(pivot, Vector3.up);

        emitter.transform.position =
        new Vector3(Mathf.Clamp(emitter.transform.position.x, -1f, 1f), emitter.transform.position.y, emitter.transform.position.z);

        if (Input.GetMouseButtonDown(0))
        {
            startTimer = Time.unscaledTime;
            deltaTimer = 0f;
            rotateMugTween = mug.DOLocalRotate(new Vector3(90f, 0f, 0f), GameConfiguration.Instance.fluidDelay).SetEase(Ease.Flash);
        }
        else
        if (Input.GetMouseButton(0))
        {
            deltaTimer = Time.unscaledTime - startTimer;
            if (deltaTimer >= GameConfiguration.Instance.fluidDelay)
            {
                emitter.speed = GameConfiguration.Instance.fluidSpeed;
            }
        }
        else
        if (Input.GetMouseButtonUp(0))
        {
            emitter.speed = 0f;
            deltaTimer = 0f;
            rotateMugTween.Kill();
            mug.DOLocalRotate(new Vector3(0f, 0f, 0f), 0.2f);
        }
        timerFill.fillAmount = deltaTimer / GameConfiguration.Instance.fluidDelay;
    }

    #endregion

    #region DEBUG
    #endregion

}
